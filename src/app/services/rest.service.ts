import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import Mailgun from 'mailgun.js';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  private urlBeta = 'https://ca-prodback-mf.allamericanmbs.com/mf25_ca/public/api';
  private tokenSubject: BehaviorSubject<string | null>;

  constructor(private http: HttpClient) {
    const token = localStorage.getItem('access_token');
    this.tokenSubject = new BehaviorSubject<string | null>(token);
  }

  login(): Observable<any> {
    return this.http.post(this.urlBeta + '/login', {
      email: 'victor@allamericanmbs.com',
      password: '123456789'
    }).pipe(
      tap((response: any) => {
        const token = response.access_token;
        const expiresAt = response.expires_at;

        // Verifica que el token y la fecha de expiración se están guardando correctamente
        console.log('Token:', token);
        console.log('Expires At:', expiresAt);

        // Guarda el token y la fecha de expiración en localStorage
        localStorage.setItem('access_token', token);
        localStorage.setItem('expires_at', expiresAt);

        this.tokenSubject.next(token);
      })
    );
  }

  private isTokenValid(): boolean {
    const expiresAt = localStorage.getItem('expires_at');
    if (!expiresAt) {
      return false;
    }
    const now = new Date();
    const expirationDate = new Date(expiresAt);
    console.log('Token expiration date:', expirationDate);
    return expirationDate > now;
  }

  getCategories(): Observable<any> {
    if (this.isTokenValid()) {
      const token = this.tokenSubject.getValue();
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
      return this.http.get(this.urlBeta + '/inventory-categories', { headers });
    } else {
      throw new Error('User is not authenticated or token has expired');
    }
  }

  getItems(): Observable<any> {
    if (this.isTokenValid()) {
      const token = this.tokenSubject.getValue();
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
      return this.http.get(this.urlBeta + '/inventories', { headers });
    } else {
      throw new Error('User is not authenticated or token has expired');
    }
  }

  sendForm(data: any): Observable<any> {
    if (this.isTokenValid()) {
      const token = this.tokenSubject.getValue();
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
      return this.http.post(this.urlBeta + '/create-estimate-estimator', data, { headers });
    } else {
      throw new Error('User is not authenticated or token has expired');
    }
  }

  sendEmail(link: string, receiver: string) {
    const owner = 'erika@acero.industries' + ',' + 'oscar@allamericanbuildings.com';
    const API_KEY = 'fe25a66a8a9b0076a1afb466a465b3cc-835621cf-ac42817d';
    const DOMAIN = 'allamericanmbs.com';
    const formData = require('form-data');
    const mailgun = new Mailgun(formData);
    const client = mailgun.client({ username: 'api', key: API_KEY });

    const message = {
      from: 'Acero Industries <marketing@acero.industries>',
      to: [receiver],
      cc: owner,
      subject: 'Quote Request',
      html: this.renderMessageMail(link)
    };
    client.messages.create(DOMAIN, message)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderMessageMail(link: string) {
    const html = `<div style='text-align:center'> 
      <h1 style='color: black'>Thank you for placing your quote request with Acero Building Components! 
      Attached is a copy of your estimate. One of our representatives will be in touch with you soon!</h1>
      <img src='https://storage.googleapis.com/allamericanmbs/assets/Acero.png' style='width:50%'><br>
      <p style='font-size: x-large; font-weight:bold; color: black'>Click here to review your quote<br> 
      <a href='${link}' style='font-size: large; font-weight:bold;'>CLICK HERE!</a></p> 
      <p style='font-size: large; font-weight: bold; color: black'>Contact us</p> 
      <p style='font-size: large; color: black;'>Phone: 
      <a href='tel:530-458-5800' style='font-size: large'>530-458-5800</a></p> 
      <p style='font-size: large; color: black;'>Email: 
      <a href='mailto:erika@acero.industries' style='font-size: large'>erika@acero.industries</a></p> 
      <p style='font-size: large; font-weight: bold; color:black'>Do not reply to this email</p> 
      </div>`;
    return html;
  }
}