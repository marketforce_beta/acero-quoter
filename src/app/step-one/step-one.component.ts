import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { NavigationExtras, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
declare var gtag:any;

@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.scss']
})
export class StepOneComponent implements OnInit {
  resources: any = [];
  resourcesProducts: any = [];
  resourcesCategories: any = [];
  items:any = [];
  category:any = [];
  products:any = [];
  currentCategory:any = null;
  currentProduct:any = null;
  currentPrice:any = null;
  currentLength:any = null;
  currentQty:any = null;
  isActive: boolean = true;
  dataOrder:any = {};
  buttonSendState:boolean = false;

  total:number = 0;
  constructor(
    private restServices: RestService,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.initialize();
  }

  initialize() {
    if (this.restServices['isTokenValid']()) {
      this.loadItems();
      this.loadCategories();
    } else {
      this.login();
    }
  }

  login(){
    this.restServices.login().subscribe(
      response => {console.log('Login successful');
      },
      error => {console.log('Login failed', error);}
    );
  }

  loadItems(){
    this.restServices.getItems()
    .subscribe(response => {
      if(response.data !== null){
        this.resourcesProducts = response.data;
      }
    });
  }

  loadCategories(){
    this.restServices.getCategories()
    .subscribe(response => {
      if(response.data !== null){
        this.resourcesCategories = response.data;
        console.log('Resources:', this.resources);
        this.loadCategory();
      }
    });
  }

  loadCategory() {
    const temp: any = [];
    this.resourcesCategories.forEach((element: any) => {
      // if (element.name === 'Products') {
      //   temp.push({ id: element.id, name: 'Accessories' });
      // } else {
        temp.push({ id: element.id, name: element.name });
      // }
    });  
    this.category = temp.reduce((acc: any, item: any) => {
      if (!acc.some((obj: any) => obj.id === item.id)) {
        acc.push(item);
      }
      return acc;
    }, []);
    // Filtra los primeros 5 elementos
    this.category = this.category.slice(0, 6);
  }

  loadProducts(){
    const excludedId = 1;
    const exclusedName = 'CUSTOM GARAGE DOOR';
    const excludedName2 = 'ANCHORS';
    const temp:any = [];
    this.resourcesProducts.forEach((element:any )=> {
      if(element.inventory_category_id === this.currentCategory && element.id !== excludedId && element.name !== exclusedName && element.name !== excludedName2){
        temp.push({id:element.id, name: element.name, type: element.unit_name, price: element.price});
      }
    });
    this.products = temp;
  }

  validateLength() {
    const found = this.products.find((element:any) => element.id === this.currentProduct);
    if (found) {
      if (found.type === 'Piece') {
        this.isActive = true;
      } else {
        this.isActive = false;
      }
    }
  }

  addItem(){
    const item = {
      category: this.category.find((element:any) => element.id === this.currentCategory).name,
      product: this.products.find((element:any) => element.id === this.currentProduct).name,
      length: this.currentLength,
      qty: this.currentQty,
      unit: this.products.find((element:any) => element.id === this.currentProduct).price,
      price: this.caclulateCost()
    }
    this.items.push(item);
    this.total = 0;
    this.total = parseFloat(this.calculateTotal());
    this.cleanData();
  }

  caclulateCost(){
    const price = this.resourcesProducts.find((element:any) => element.id === this.currentProduct).price;
    const found = this.resourcesProducts.find((element:any) => element.id === this.currentProduct);
    if(found.unit_name === 'Foot'){
      return price * this.currentLength * this.currentQty;
    }
    return price * this.currentQty;
  }

  calculateTotal(){
    let total:number = 0;
    this.items.forEach((element:any) => {
      total += element.price;
    }
    );
    return total.toFixed(2);
  }

  cleanData(){
    this.products = [];
    this.currentCategory = null;
    this.currentProduct = null;
    this.currentPrice  = null;
    this.currentLength = null;
    this.isActive = true;
    this.currentQty = null;
  }

  completeNumber(number:number){
    if(number === undefined || number === null){
      return '';
    }
    let num = String(number); ;
    let item = num.split('.');
    if(item.length > 1){
      if(item[1].length > 1){
        return item.join('.');
      }else{
        item[1] = item[1]+ '0';
        return item.join('.');
      }

    }
    return num+'.00';
  }

  sendQuote(){
    const queryParams:any = {}
    const strData = JSON.stringify(this.items);
    queryParams.myArray = strData;
    const navigation: NavigationExtras ={
      queryParams
    }
    this.router.navigate(['/send'],navigation);
  }

  confirmSend(){

  }

  deleteItem(index:number){
    this.items.splice(index, 1);
    this.total = 0;
    this.total = parseFloat(this.calculateTotal());
  }

  generateQuote(data:FormData){
    if(this.items.length === 0){
      Swal.fire({
        title: 'Something went wrong!',
        text: 'You need to add at least one item to your quote',
        icon: 'warning'
      });
    }else{
      if(this.validateForm(data)){
        Swal.fire({
          title: 'Are you sure?',
          text: 'You will be contacted to review and place your order, once the quote is received!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#cf202a',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.buttonSendState = !this.buttonSendState;
            const form = JSON.parse(JSON.stringify(data));
            this.dataOrder.name = form.name;
            this.dataOrder.email = form.email;
            this.dataOrder.phone = form.phone;
            this.dataOrder.items = this.items;
            this.dataOrder.total = Number(this.calculateTotal());            
            this.restServices.sendForm(this.dataOrder).subscribe(response => {
              if(response.data !== null){
                this.sendEmailQuoter(response.data.pdf, form.email);
                this.showConfirm();
                this.router.navigate(['/send']);
                this.trackMe();
              }
            });
          }
        })
      }
    }
  }

showConfirm(){
  let timerInterval:any;
  Swal.fire({
    title: 'Thank you for the information!',
    timer: 2000,
    timerProgressBar: true,
    didOpen: () => {
      Swal.showLoading()
    },
    willClose: () => {
      clearInterval(timerInterval)
    }
  }).then((result) => {
    this.router.navigate(['/send']);
  })
}

validateForm(data:FormData){
  const form = JSON.parse(JSON.stringify(data));

  if(form.name === ''){
    Swal.fire({
      title: 'Please enter your name',
      icon: 'error'
    })
    return false;
  }
  if(form.email === ''){
    Swal.fire({
      title: 'Please enter your email',
      icon: 'error'
    })
    return false;
  }
  if(form.phone === '' || form.phone.length < 10){
    Swal.fire({
      title: 'Please verify your phone',
      icon: 'error'
    })
    return false;
  }
  if(!this.validateEmail(form.email)){
    Swal.fire({
      title: 'Please enter a valid email',
      icon: 'error'
    })
    return false;
  }
  return true;
}

sendEmailQuoter(link:string, receiver:string){
  this.restServices.sendEmail(link, receiver);
}

validateEmail(email:string){
  let emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  return (emailRegex.test(email)) ? true: false;
}

trackMe() {
    gtag('event', 'conversion', {'send_to': 'AW-10963012682/S8rwCNrPnIoYEMqYyeso'});
  }
}
