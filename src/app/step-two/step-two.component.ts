import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from '../services/rest.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
export class StepTwoComponent implements OnInit {
  items:any= [];
  dataOrder:any = {};
  constructor(
    private activatedRoute: ActivatedRoute,
    private restService: RestService,
    private router: Router,
    private http: HttpClient
    ) {
    }

  ngOnInit(): void {
  }

  backToHome(){
    this.router.navigate(['/']);
  }
}
