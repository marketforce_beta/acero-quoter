document.addEventListener('DOMContentLoaded', function() {
document.getElementById('footer-placeholder').innerHTML = `
<footer style="background-color: black;"  id="footer-placeholder">
		<div class="container" style="background-color: black;">
			<div class="footerrowflexacero">
				<div class="listoptionsfooter">
					<li><a href="https://acero-ca.com/">Home</a></li>
				</div>
					<div class="hr6"></div>
				<div class="listoptionsfooter">
					<li><a href="https://acero-ca.com/about_us">About Us</a></li>
				</div>
					<div class="hr6"></div>
			    <div class="listoptionsfooter">
					<li><a href="https://acero-ca.com/resources">Resources</a></li>
				</div>
					<div class="hr6"></div>
				<div class="listoptionsfooter">
					<li><a href="https://acero-ca.com/products">Products</a></li>
				</div>
					<div class="hr6"></div>	
				<div class="listoptionsfooter">
					<li><a href="https://acero-ca.com/contact_us">Contact Us</a></li>
				</div>
					<div class="hr6"></div>	
				
					<a href="tel:5304585800" onclick="llamar()"><i class="fa fa-phone"></i>530-458-5800</a>
				
			</div>
			<div class="hr5"></div>
			<div class="hr6"></div>	
			<div class="footerrowflexacero2web">
				<div class="socialfootermedialinks">
					<a href="https://www.facebook.com/AceroCABuildingComponents" target="_blank"><img src="assets/facebook_white.png" alt="Facebook" width="30" height="30" style="margin-right: 10px;"></a>
                    <a href="https://www.instagram.com/acerobuildingcomponentsca" target="_blank"><img src="assets/instagram_white_new.png" alt="Instagram" width="31" height="31"></a>
				</div>
				<div class="copyrightfooter">
					<p>© Copyright Acero CA Buildings Components. All Rights Reserved</p>
				</div>
				<div class="logoacerofooter">
					<a href="index"><img src="assets/acero_white.png" alt="Acero Logo" width="160" height="40"></a>
				</div>
			</div>

			<div class="footerrowflexacero2mobile">
			    <div class="logoacerofootermobile">
					<a href="index"><img src="assets/acero_white.png" alt="Acero Logo" width="160" height="40"></a>
					<div class="socialfootermedialinksmobile">
						<a href="https://www.facebook.com/AceroCABuildingComponents" target="_blank"><img src="assets/facebook_white.png" alt="Facebook" width="30" height="30" style="margin-right: 10px;"></a>
                    	<a href="https://www.instagram.com/acerobuildingcomponentsca" target="_blank"><img src="assets/instagram_white_new.png" alt="Instagram" width="30" height="30"></a>
					</div>
				</div>
				<div class="copyrightfooter">
					<p>© Copyright Acero CA Buildings Components. All Rights Reserved</p>
				</div>
				
			</div>
		</div>
	</footer>
`;
});