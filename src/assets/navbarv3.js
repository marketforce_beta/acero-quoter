document.addEventListener('DOMContentLoaded', function() {
document.getElementById('header-placeholder').innerHTML = `
<header class="header" id="header-placeholder">

<div class="topbar">
      <a href="tel:5304585800" onclick="llamar()"><i class="fa fa-phone"></i>530-458-5800</a>
      <a href="https://maps.app.goo.gl/dGwDD3xocSoFC75r6" target="new"><i class="fa fa-map-marker"></i>2890 Niagara Ave. Colusa, CA 9532</a>
</div>

      <nav class="nav2">
        <a class="logo-bar" href="https://acero-ca.com/"><img src="assets/acero_white.png" alt="Acero California logo"></a>
          <button class="nav-toogle" aria-label="Open menu">
            <i class="fa-solid fa-bars icono-blog"></i>
          </button>
        <ul class="nav-menu2">
            <li class="nav-menu-item">
              <a href="https://acero-ca.com/" class="nav-menu-link nav-link2">Home</a>
            </li>
            <li class="nav-menu-item">
              <a href="https://acero-ca.com/about_us" class="nav-menu-link nav-link2">About Us</a>
            </li>
            <li class="nav-menu-item">
              <a href="https://acero-ca.com/resources" class="nav-menu-link nav-link2">Resources</a>
            </li>
            <li class="nav-menu-item dropdown hassubmenu">
              <a href="https://acero-ca.com/products" class="dropdown-toggle nav-link2" data-toggle="dropdown" role="button" aria-expanded="false">Products<span class="fa fa-angle-down"></span></a>
                <ul class="dropdown-menu">                  
                  <li><a href="https://acero-ca.com/metal_panels">Metal Panels</a></li>
                  <li><a href="https://acero-ca.com/skylight_panels">Skylight Panels</a></li>
                  <li><a href="https://acero-ca.com/tubing">Galvanized Tubing</a></li>
                  <li><a href="https://acero-ca.com/roll_up_door">Roll Up Doors</a></li>
                  <li><a href="https://acero-ca.com/trim">Custom Trims</a></li>
                  <li><a href="https://acero-ca.com/insulation">Insulation</a></li>
                  <li><a href="https://acero-ca.com/window">Windows</a></li>
                  <li><a href="https://acero-ca.com/screws">Screws</a></li>
                  <li><a href="https://acero-ca.com/anchors">Anchors</a></li>
                  <li><a href="https://acero-ca.com/walk_in_doors">Walk in Doors</a></li>
                </ul>
            </li>
            <li class="nav-menu-item">
              <a href="https://acero-ca.com/contact_us" class="nav-menu-link nav-link2">Contact us</a>
            </li>
            <li class="nav-menu-item">
              <a href="https://estimator.acero-ca.com/" target="new" class="btn btn-primary navbar-button">Estimator Tool</a>
            </li>
          </ul>
      </nav>
</header>
`;

const navToogle = document.querySelector('.nav-toogle');
const navMenu = document.querySelector('.nav-menu2');
const navbar = document.querySelector('.nav2');
const topbar = document.querySelector('.topbar');

// Aplicar estilos iniciales al cargar la página
if (window.scrollY > 0) {
  navbar.classList.add('opaque');
}
if (window.scrollY > 100) {
  topbar.style.display = "none";
}

navToogle.addEventListener('click', () => {
    navMenu.classList.toggle('nav-menu_visible');
    if (navMenu.classList.contains('nav-menu_visible')) {
        navToogle.setAttribute('aria-label', 'Close menu');
    } else {
        navToogle.setAttribute('aria-label', 'Open menu');
    }
});

window.addEventListener("scroll", function() {
    if (window.scrollY > 0) {
        navbar.classList.add('opaque');
    } else {
        navbar.classList.remove('opaque');
    }

    if (window.scrollY > 100) {
      topbar.style.display = "none";
    } else {
      topbar.style.display = "block";
    }

    if (window.innerWidth < 1000) { // Comprobar si la resolución es menor a 1000px
        if (window.scrollY > 100) {
            navMenu.style.top = '60px'; // Si se ha hecho scroll, establecer top en 0
        } else {
            navMenu.style.top = '110px'; // Si no se ha hecho scroll, establecer top en 110px
        }
    }
});
});